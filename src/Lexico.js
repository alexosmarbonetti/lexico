const symbolTable = require('./symbolTable.json')
const { readFileSync } = require('fs')

class Lexico {
    constructor () {
        this.symbolTable = symbolTable
    }
    findLexeme (symbol, line) {
        const filters = {
            char: new RegExp(/^[a-z]*$/g).test(symbol),
            number: new RegExp(/^[0-9]*$/g).test(symbol),
            decl: {
                let: new RegExp(/^let$/g).test(symbol),
                const: new RegExp(/^const$/g).test(symbol)
            },
            laco: new RegExp(/^while$/g).test(symbol),
            condicional: new RegExp(/^if$/g).test(symbol),
            id: new RegExp(/^[a-z-0-9]*$/g).test(symbol),
            comparador: new RegExp(/^[<>=]$/g).test(symbol),
            logico: {
                or: new RegExp(/^or$/g).test(symbol),
                and: new RegExp(/^and$/g).test(symbol),
            },
            op_arit: new RegExp(/^[+--*/]$/g).test(symbol)
        }
        return this.newRankLexeme(filters, symbol, line)
    }
    newRankLexeme(filters, symbol, line) {
        if (filters.condicional) {
            return { token: 'CONDICIONAL', lexeme: symbol, line }
        } else if (filters.decl.let) {
            return { token: 'DECL', lexeme: symbol, line }
        }else if (filters.decl.const) {
            return { token: 'DECL', lexeme: symbol, line }
        }else if (filters.laco) {
            return { token: 'LACO', lexeme: symbol, line }
        }else if (filters.number) {
            return { token: '[0-9]', lexeme: symbol, line }
        }else if (filters.op_arit) {
            return { token: 'OP_ARIT', lexeme: symbol, line }
        }else if (filters.comparador) {
            return { token: 'COMPARADOR', lexeme: symbol, line }
        }else if (filters.logico.or) {
            return { token: 'LOGICO', lexeme: symbol, line }
        }else if (filters.logico.and) {
            return { token: 'LOGICO', lexeme: symbol, line }
        }else if (filters.id) {
            return { token: '[A-Z]', lexeme: symbol, line }
        } else {
            return 'Invalid symbol'
        }
    }
}

module.exports = Lexico