const Lexico = require('./Lexico')
const Queue = require('./utils/Queue')
const Stack = require('./utils/Stack')
const { getSymbol } = require('./sintaticTable')
const lexicalAnalyser = new Lexico()
const stack = new Stack()
const queue = new Queue()

module.exports.Sintatic = class {
    #lexicalElements = []
    constructor (code) {
        console.log(code)
        //code = code.split(' ')
        code.forEach(c => queue.enqueue(c))
        this.#lexicalElements = code
        console.log(this.#lexicalElements)
        queue.enqueue('$')
        stack.push('$')
        stack.push('S')
    }
    analyse () {
        while(true) {
            const symbol = queue.getFirst()
            if (symbol === '$' && stack.getFirst() === '$') {
                console.log('Compilation Success. Found X warnings')
                break
            }
            if (!symbol) {
                throw new Error('The entry is blank')
            }
            //const lexicalAnalisis = lexicalAnalyser.findLexeme(symbol)
            //if (lexicalAnalisis === 'Invalid symbol') {
              //  throw new Error(`Lexical error, ivalid symbol ${symbol} in code`)
            //}
            const copy = this.classifySymbol(symbol)
            queue.replace(symbol, copy)
            if (copy === stack.getFirst()) {
                queue.dequeue()
                stack.pop()
            } else {
                //Init analisis
                const production = getSymbol(stack.getFirst(), copy)
                if (production && production !== 'Sync' && production !== '&') {
                    stack.pop()
                    let elem = stack.reverseString(production)
                    elem.forEach(e => stack.push(e))
                    console.log('Pilha: ', stack.getStack())
                } 
                if (production && production === 'Sync') {
                    console.log('Warning... An error occurred, but the algorithm successfully recovered')
                }
                if (!production) {
                    throw new Error('Fatal error, recovery is impossible. Fatal sintatic error')
                }
            }
        }
    }
    
    classifySymbol (symbol) {
        const exp = new RegExp(/^[a-z-0-9]*$/g).test(symbol)
        if (exp && symbol !== 'let' && symbol !== 'const' && symbol !== 'loop' && symbol !== 'if') {
            if (isNaN(symbol)) {
                return '[A-Z]'
            }
            return '[0-9]'
        }
        return symbol
    }
}