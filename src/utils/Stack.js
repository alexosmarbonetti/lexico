const Stack = class {
    #elements
    constructor (elements) {
        this.#elements = []
    }

    push(element) {
        console.log(element)
        return this.#elements.push(element)
    }

    pop (element) {
        return this.#elements.pop()
    }

    reverseString (string) {
        return string.split(' ').reverse()
    }

    getFirst () {
        return this.#elements[this.#elements.length - 1]
    }

    getStack () {
        return this.#elements
    }
}

module.exports = Stack