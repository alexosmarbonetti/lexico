const Queue = class {
    #queue
    constructor() {
        this.#queue = []
    }
    enqueue(element) {
        this.#queue.push(element)
    }
    dequeue() {
        return this.#queue.shift()
    }
    getFirst () {
        return this.#queue[0]
    }
    getQueue () {
        return this.#queue
    }

    replace (symbol, replacer) {
        const index = this.#queue.indexOf(symbol)
        if (index !== -1) {
            this.#queue[index] = replacer
        }
    }
}

module.exports = Queue