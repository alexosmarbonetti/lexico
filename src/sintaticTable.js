const table = []

//Posso remover o num_aux e o id_aux e deixar ID = [A-Z] e NUM = [0-9]
table["S"] = []
table["S"]["let"] = "DECL ID : NUM"
table["S"]["const"] = "DECL ID : NUM"
table["S"]["loop"] = "RESERVEDS"
table["S"]["if"] = "RESERVEDS"

//DECL
table["DECL"] = []
table["DECL"]["let"] = "let"
table["DECL"]["const"] = "const"

//ID
table["ID"] = []
table["ID"]["[A-Z]"] = "[A-Z]"

//NUM
table["NUM"] = []
table["NUM"]["[0-9]"] = "[0-9]" //So 0-9
table["NUM"]["$"] = "Sync"

//RESERVEDS
table["RESERVEDS"] = []
table["RESERVEDS"]["if"] = "if ( IF_EXP ) { RESERVEDS }"
table["RESERVEDS"]["loop"] = "LACO ( LOOP_EXP ) { OPERACAO }"

//IF_EXP
table["IF_EXP"] = []
table["IF_EXP"]["[A-Z]"] = "ID IF_EXP'"

//IF_EXP'
table["IF_EXP'"] = []
table["IF_EXP'"]["<"] = "COMPARADOR ID"
table["IF_EXP'"][">"] = "COMPARADOR ID"
table["IF_EXP'"]["="] = "COMPARADOR ID"
table["IF_EXP'"]["and"] = "LOGICO ID"
table["IF_EXP'"]["or"] = "LOGICO ID"

//LOOP_EXP
table["LOOP_EXP"] = []
table["LOOP_EXP"]["[0-9]"] = "NUM COMPARADOR NUM"

//LACO
table["LACO"] = []
table["LACO"]["loop"] = "loop"

//OPERACAO
table["OPERACAO"] = []
table["OPERACAO"]["[A-Z]"] = "ID OP_ARIT OPERACAO'"

//OPERACAO'
table["OPERACAO'"] = []
table["OPERACAO'"]["[A-Z]"] = "ID"
table["OPERACAO'"]["[0-9]"] = "NUM"

//COMPARADOR
table["COMPARADOR"] = []
table["COMPARADOR"]["<"] = "<"
table["COMPARADOR"][">"] = ">"
table["COMPARADOR"]["="] = "="

//LOGICO
table["LOGICO"] = []
table["LOGICO"]["and"] = "and"
table["LOGICO"]["or"] = "or"

//OP_ARIT
table["OP_ARIT"] = []
table["OP_ARIT"]["+"] = "+"
table["OP_ARIT"]["-"] = "-"
table["OP_ARIT"]["*"] = "*"
table["OP_ARIT"]["/"] = "/"

module.exports.getSymbol = (elemPilha, elemFila) => {
    return table[elemPilha][elemFila]
}