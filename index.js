const { Sintatic } = require('./src/Sintatico')

//const sintatic = new Sintatic('loop ( 5 = 5 ) { x + y }')

//sintatic.analyse()

const { readFile } = require('fs')

const getFileData = () => {
    return new Promise((resolve, reject) => {
        readFile('./inputs/input.txt', (err, data) => {
            if (err) {
                reject(err)
            }
            resolve(data.toString())
        })
    })
}

const main = async () => {
    const data = await getFileData()
    let dataArray = data.split(' ')
    dataArray = dataArray.map(c => c.replace('\r', '').replace('\n', ''))
    const code = dataArray.filter(c => c !== '')
    const index = code.indexOf('}}')
    code[index] = '}'
    console.log(code)
    const sintatic = new Sintatic(code)
    sintatic.analyse()
}

main()
